// DedupObejct.cpp

#include <utility>
#include <cstdlib>
#include <cstdio>
#include <fstream>
#include <iostream>
#include <sstream>
#include <iterator>
#include <vector>
#include <string>
#include <map>
#include <cmath>
#include <sstream>
#include <iomanip>
using namespace std;

#include "DedupObject.h"


// DedupObject constructor
DedupObject::DedupObject() {}


// find_left_most
DedupObject::Node* DedupObject::find_left_most(Node* r) {


	Node* l = r;
	if ( l != NULL ) {

		while ( l->m_left != NULL ) l = l->m_left;
	}

	return l;

}
