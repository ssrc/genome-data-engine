// HashValues.h

#include <utility>
#include <cstdlib>
#include <cstdio>
#include <fstream>
#include <iostream>
#include <sstream>
#include <iterator>
#include <vector>
#include <string>
#include <map>
#include <cmath>
#include <sstream>
#include <iomanip>
using namespace std;

#include "HashValues.h"
#include "Similarity.h"

// HashValues constructor 
HashValues::HashValues(size_t size, Similarity sim, const string hash_seq ):
	m_table_size(size), 
    m_sim(sim),
	m_hash_seq(hash_seq) {

    m_seq_length = m_hash_seq.size();

}

HashValues::HashValues () { }


// compute_hash()
size_t HashValues::compute_hash(double relative_sim) {
	
	double hash = (size_t)(relative_sim * m_table_size) % (m_table_size-1);

	//double hash = (size_t)(relative_sim * m_table_size) % (m_table_size);

	size_t hash_value = (size_t) hash;
	hash_value += 1;
	return hash_value;
}


// DedupLicbrary Constructor	
DedupLibrary::DedupLibrary( Library& library ) : m_library(library) {


	dedup_obj m_brca1_objs  = {
		{ 1, "GTACCTTGATTTCGTGTTCTGAGAGGCTGCTGCTTAGCAGTAGCCCCTTG" },
		{ 2, "GTACCTTGATCTCGTATTCTGAGAGACTGCTGCTTAGCCGTAGCCCCTTG" },
		{ 3, "ATACCTTGCTTTCGTATTGTGAGAGGCTGCTGCTTAGCGGTAGCTCCTTG" },
		{ 4, "GTACCATGATTTCGTACTCTGAGAGACTGCTGCTTAGAGGTAGCGCCTTG" } 

	};

	gene brca1 = {{"brca1", m_brca1_objs}};


	chromosome c1, c2, c3, c4, c5, c6, c7, c8, c9, c10, c11, c12, c13,  
               c14, c15, c16, c17, c18, c19, c20, c21, cx, cy;

	c17 = {{ 17, brca1 }};

	library = { c1, c2, c3, c4, c5, c6, c7, c8, c9, c10, c11, c12, c13, 
				c14, c15, c16, c17, c18, c19, c20, c21, cx, cy };


}  

// load()
chromosome DedupLibrary::load(chromosome chr ) {

	return chr;
}

// print_library()
void DedupLibrary::print_library() {

    Library::iterator lib_iter = m_library.begin();
    cout << "You have entered the library: " << endl;
	while ( lib_iter != m_library.end() ) { 
        chromosome::iterator chrom_iter = lib_iter->begin();
        while ( chrom_iter != lib_iter->end() ) { 
            int32_t chrom = chrom_iter->first;
            cout << "\t Chromosome # = " << chrom << endl;
            gene::iterator gene_iter = chrom_iter->second.begin();
            cout << "\t\t Genes located: " << endl;
            while ( gene_iter != chrom_iter->second.end() ) { 
                cout << "Here are your objects for: " 
                << gene_iter->first   << endl;
                dedup_obj::iterator ddo_iter = gene_iter->second.begin();
                while ( ddo_iter != gene_iter->second.end() ) { 
                    cout << ddo_iter->first << " " <<ddo_iter->second << endl;
                    ddo_iter++;
                }   
                gene_iter++;
            }   
            chrom_iter++;
        }   
        lib_iter++;
    }   

}


void DedupLibrary::insert(string seq, string gene ) {

	string prim_seq = get_primary_seq(gene);
	Similarity sim(seq, prim_seq);
	double rel_sim = sim.angular_distance();
	HashValues hv (57, sim, seq); 
	
	size_t hash_value = hv.compute_hash(rel_sim);	
	Library::iterator lib_iter = m_library.begin();
	while ( lib_iter != m_library.end() ) { 
        chromosome::iterator chrom_iter = lib_iter->begin();
        while ( chrom_iter != lib_iter->end() ) { 
            gene::iterator gene_iter = chrom_iter->second.begin();
            while ( gene_iter != chrom_iter->second.end() ) { 
				if ( gene_iter->first == gene ) { 
					cout << hash_value << endl;
					pair<size_t,string> p (hash_value,seq);
					gene_iter->second.insert(p);
						
				}
                else {
					cout << "unmatched gene in the library" << endl;
				}
				gene_iter++;
            }   
            chrom_iter++;
        }   
        lib_iter++;
    }   

}



string DedupLibrary::get_primary_seq(string gene ) {

	string seq;
	Library::iterator lib_iter = m_library.begin();
	while ( lib_iter != m_library.end() ) {
		chromosome::iterator chrom_iter = lib_iter->begin();
		while ( chrom_iter != lib_iter->end() ) {
			gene::iterator gene_iter = chrom_iter->second.begin();
			while ( gene_iter != chrom_iter->second.end() ) {
				if ( gene_iter->first == gene ) {
					dedup_obj ddo = gene_iter->second;
					dedup_obj::iterator ddo_iter = ddo.begin();
					if ( ddo_iter != ddo.end() ) {
						seq = ddo_iter->first;
						return seq;
					}
				}
				gene_iter++;
			}
			chrom_iter++;
		}
		lib_iter++;
	}

	return ("gene could not be located");
} 


/*void bubble_sort( dedup_obj ddo ) {

	int n = ddo.size();
	n -= 1;

	dedup_obj::iterator iter = ddo.begin();
	dedup_obj::iterator itup = ddo.upper_bound(n);
	for(; iter != ddo.end(); iter++ ) {
		dedup_obj::iterator iter2 = ddo.begin();
		for (; iter2 != itup; iter2++ ) {
			if (iter2->second > iter2->second + 1 ) {
				int temp = iter2->second + 1;
				iter2->second + 1 = iter2->second;
				iter2->second = temp; 
		}
	}

}*/


//
// Deduplication Library Iterator inner class
//

DedupLibrary::DLIterator::DLIterator(DedupLibrary& dl): 
	
	 m_p_dl(&dl), m_lib_iter (dl.m_library.begin())   {
	
	while ( m_lib_iter != m_p_dl->m_library.end() ) {
		m_chrom_iter = m_lib_iter->begin();
		while ( m_chrom_iter != m_lib_iter->end() ) {
			m_gene_iter = m_chrom_iter->second.begin();
			while (	m_gene_iter != m_chrom_iter->second.end() ) {
				m_ddo_iter = m_gene_iter->second.begin();
				if ( m_ddo_iter != m_gene_iter->second.end() ) {
					return;
				}
				m_ddo_iter++;
			}
			m_gene_iter++;
		}
		m_chrom_iter++;
	}
	m_lib_iter++;
}


DedupLibrary::DLIterator::DLIterator(){}


bool DedupLibrary::DLIterator::operator== ( const DLIterator& rhs ) const {
	return m_lib_iter == rhs.m_lib_iter and
		   m_chrom_iter == rhs.m_chrom_iter and 
		   m_gene_iter == rhs.m_gene_iter and
		   m_ddo_iter == rhs.m_ddo_iter;
}

bool DedupLibrary::DLIterator::operator!= ( const DLIterator& rhs ) const {

	return !this->operator==(rhs);

}
 	

DedupLibrary::DLIterator& DedupLibrary::DLIterator::operator++ ( int ) {
	m_next();
	return *this;
}


void DedupLibrary::DLIterator::m_next() {

	while ( m_lib_iter != m_p_dl->m_library.end() ) {

		while ( m_chrom_iter != m_lib_iter->end() ) {
	
			while ( m_gene_iter != m_chrom_iter->second.end() ) {

				if ( m_ddo_iter != m_gene_iter->second.end() ) return;
				m_ddo_iter++;
				
				/*if ( m_gene_iter != m_lib_iter.end() ) {
					m_chrom_iter = m_lib_iter.begin();
					m_ddo_iter = m_gene_iter.begin();
				}*/

			}
			if ( m_gene_iter != m_chrom_iter->second.end() ) return;
			m_gene_iter++;		
		}
		if ( m_chrom_iter != m_lib_iter->end() ) return;
		m_lib_iter++;	
	} 
}

DedupLibrary::LibraryInfo DedupLibrary::DLIterator::operator*() {

	return DedupLibrary::LibraryInfo( m_chrom_iter->first, 
						m_ddo_iter->second, m_ddo_iter->first);

}

DedupLibrary::DLIterator DedupLibrary::begin() {

	return DLIterator(*this);
}

DedupLibrary::DLIterator DedupLibrary::end() {

	return DLIterator();
}


