// Similarity.h

#ifndef __SIMILARITY_H_
#define __SIMILARITY_H_

#include <cstdio> 
#include <string>
#include <stdexcept>
#include <utility>
#include <iostream>
#include <list>
using namespace std;


class Similarity {


	private:

		const string m_seq1;
		const string m_seq2;
		long double difference(string, string);
		vector<vector<size_t>> get_matrix();
		size_t compute_edit_distance();
		double compute_median(size_t, size_t, size_t);


	public:

		Similarity( const string&, const string&);
		Similarity();
		pair<bool,bool> check_if_purine(char,char);
		size_t substitution(char,char);	
		void print_matrix();
		long double diff;
		double measure_soft_cosine();
		size_t edit_distance;	
		vector<vector<size_t>> get_matrix(vector<vector<size_t>>);
		vector<vector<size_t>> m_matrix;
		double inverse();
		double angular_distance();
		double convert_to_degrees();
		const string get_hash_seq();
};




#endif
