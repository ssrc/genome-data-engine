// HashValues.h

#ifndef __HASHVALUES_H_
#define __HASHVALUES_H_

#include <utility>
#include <cstdlib>
#include <cstdio>
#include <fstream>
#include <iostream>
#include <sstream>
#include <iterator>
#include <vector>
#include <string>
#include <map>
#include <cmath>
#include <sstream>
#include <iomanip>
using namespace std;

#include "Similarity.h"

typedef map<int32_t, string> dedup_obj;
typedef map<string, dedup_obj> gene;
typedef map<int32_t, gene> chromosome;
typedef list<chromosome> Library;


class HashValues {

	public:

		size_t m_seq_length;
		size_t m_table_size;
		Similarity m_sim;
		const string m_hash_seq;
		size_t compute_hash(double);
		HashValues(size_t, Similarity, const string);
		HashValues();
};

class DedupLibrary : public HashValues {

	private:
	
		chromosome load ( chromosome );
	
	public:

		Library m_library;
		DedupLibrary( Library& );
		DedupLibrary(); 

		struct 	LibraryInfo {
			int32_t m_chromosome_num;
			string m_seq;
			int32_t m_rank;	
			LibraryInfo(int32_t num, string seq, int32_t rank):
				m_chromosome_num(num), m_seq(seq), m_rank(rank) {}
		};
		class DLIterator {
			
			private:
                DedupLibrary* m_p_dl;
				Library::iterator m_lib_iter;
                chromosome::iterator m_chrom_iter;
                gene::iterator m_gene_iter;
                dedup_obj::iterator m_ddo_iter;
                
            public: 
                DLIterator(DedupLibrary&);
                DLIterator();
                bool operator== ( const DLIterator& ) const;
                bool operator!= ( const DLIterator& ) const;
                DLIterator& operator++ ( int );    
                LibraryInfo operator*();
                void m_next();
	
		};
			
		DLIterator begin();
		DLIterator end();
		void print_library();
		void insert(string, string);	
		string get_primary_seq(string);
};


#endif
