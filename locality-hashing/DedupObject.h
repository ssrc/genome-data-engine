// Dedup.h

#ifndef __DEDUP_H_
#define __DEDUP_H_

#include <utility>
#include <cstdlib>
#include <cstdio>
#include <fstream>
#include <iostream>
#include <sstream>
#include <iterator>
#include <vector>
#include <string>
#include <map>
#include <cmath>
#include <sstream>
#include <iomanip>
using namespace std;


class DedupObject {

	public:

		string m_sequence;	
		DedupObject( string  seq) : m_sequence(seq) {}
		DedupObject();
		~DedupObject() { delete m_root; }

		//void create( const vector& );

	private:



		struct Node {
	
			int32_t m_hash_rank;	
			Node* m_left;
			Node* m_right;
			char m_gene_delta;
			Node(string seq) : m_hash_rank(1), m_left(NULL), m_right(NULL),
					         m_gene_delta('\0') {}

		}*m_root;


		Node* find_left_most(Node*);

};


#endif 

