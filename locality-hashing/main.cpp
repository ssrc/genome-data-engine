// min.cpp for the Genome Data Engine Locality Sensitive Hashing Project

#include <utility>
#include <cstdlib>
#include <cstdio>
#include <fstream>
#include <iostream>
#include <sstream>
#include <iterator>
#include <vector>
#include <string>
#include <map>
#include <cmath>
#include <sstream>
#include <iomanip>
using namespace std;

#include "Similarity.h"
#include "HashValues.h"

//#include "./"
static const size_t table_size = 5u;


pair<int32_t, string> user_spec() {

    string gene;
    int32_t chromosome;
        
    cout << "What chromosome is your gene on? " << endl;
    cin >> chromosome;
    cout << "What is your gene? " << endl;
    cin >> gene;

	pair<int32_t, string> cg = make_pair(chromosome, gene);
	return cg;

}

void view_test_library() {

	dedup_obj ddo1 = { { 1,"ACTCATC" },
					   { 2,"ACTCATG" },
					   { 3,"ACTCAAA" } };
	dedup_obj ddo2 = { {1, "ATGCGTA" },
					   {2, "ATGCGTG" },	
					   {3, "ATGCAAA" } };
	
	gene g1 = { {"gene1", ddo1 } };
	gene g2 = { {"gene2", ddo2 } };
	
	chromosome chrom1 = { { 1, g1 } };
	chromosome chrom2 = { { 2, g2 } };
	
	Library lib = { chrom1, chrom2 };	
	DedupLibrary test (lib);
	cout << endl;
	test.print_library();
	
}

void view_generic_library( string s1) {

	
	DedupLibrary dl (dl.m_library);
	dl.print_library();
	
	cout << endl;
	pair<int32_t,string> pair = user_spec();
	//int32_t chrom = pair.first; 
	string gene = pair.second;
	string seq = dl.get_primary_seq(gene);
	cout << seq << endl;
	dl.insert(s1, gene); 	
	dl.print_library();

}


void print_info( Similarity sim ) { 

    cout << "Sequence difference = " << sim.diff << endl;

    size_t ed = sim.edit_distance;
    sim.print_matrix();

    //double sc = sim.measure_soft_cosine();

    const string hash_seq = sim.get_hash_seq();

    double inv_sim = sim.inverse();
    double ad = sim.angular_distance();
    double deg = sim.convert_to_degrees();

    HashValues hvs(table_size, sim, hash_seq);
    size_t hash_value = hvs.compute_hash(ad);
    cout << "hash value for sequence = " << hash_value << endl;
	
    cout << "Edit distance = " << ed <<endl;
    //cout << "soft cosine = " << sc << endl;
    cout << "inverse sim = " << inv_sim << endl;
    cout << "angular distance= " << ad <<", " << deg 
        << " degrees" <<endl;    


}

// exit_out()
bool exit_out (int argc) {
	if ( argc != 3 ) {
		cout << "Usage: incompatible number of arguments." << endl;
		return true;
	}
	cout << "Genome Data Engine: locality " 
			"sensitive hashing process initiated" << endl;
	return false;
}

// read_arguments
pair<string,string> read_arguments(char** argv ) {

	ifstream input1(argv[1]);
	ifstream input2(argv[2]);

	pair<string,string> sp;
	if ( input1 and input2 ) { 
		// place files in strings and eliminate whitespace
		string file1, file2, hold1, hold2; 
		while ( getline(input1, hold1)) file1 += hold1;
		while ( getline(input2, hold2)) file2 += hold2;
		sp = make_pair(file1,file2);
	}
	return sp;
}

// main()
int main ( int argc, char** argv ) {

	pair<string, string> sp;
	if ( exit_out(argc) == true ) return EXIT_FAILURE;
	else sp = read_arguments(argv);	
	
	string seq1 = sp.first;
	string seq2 = sp.second;

	cout << "sequence 1 size = " << seq1.size() << endl;
	cout << "sequence 2 size = " << seq2.size() << endl;
	
	Similarity sim(seq1, seq2);

	print_info(sim);
	//print_info(sim);

	//sim.print_matrix();


	view_generic_library(seq1); 
	//view_test_library();


	return EXIT_SUCCESS;
}

