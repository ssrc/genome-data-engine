// Similarity.cpp

#include <utility>
#include <cstdlib>
#include <cstdio>
#include <fstream>
#include <iostream>
#include <sstream>
#include <iterator>
#include <vector>
#include <string>
#include <map>
#include <cmath>
#include <sstream>
#include <iomanip>
using namespace std;

#include "Similarity.h"

// Similarity() constructor
Similarity::Similarity(const string& seq1, const string& seq2) :
	m_seq1(seq1),
	m_seq2(seq2){
	diff = difference(m_seq1, m_seq2);
	m_matrix = this->get_matrix();
	edit_distance = this->compute_edit_distance();
} 

// Similarity constructor
Similarity::Similarity() {}


// difference() - finds difference btw length of two strings 
long double Similarity::difference( string s1, string s2) {
	size_t len1 = s1.size();
	size_t len2 = s2.size();
	long double diff = len1-len2;
	diff = abs(diff);
	return diff;
}


// check_if_purine()     
pair< bool, bool > Similarity::check_if_purine( char i , char j ) { 
    // A, G - purines
    // C, T - pyrimidines
    bool i_purine = true, j_purine = true;
 
    if ( i == 'A' or i == 'G' ) i_purine = true;
    else if ( i == 'C' or i == 'T' ) i_purine = false;
    if ( j == 'A' or j == 'G' ) j_purine = true;
    else if ( j == 'C' or j == 'T') j_purine = false;
    
	pair< bool, bool > boolean_pair = make_pair(i_purine, j_purine);

    return boolean_pair;
}


// Subsitution
size_t Similarity::substitution (char i , char j  ) {

    pair<bool, bool> bp = check_if_purine(i,j);

    if ( bp.first == true and bp.second == true ) {
        if ( i == j ) {
            return 0u;
        }
        else return 1u;
    }
    if ( bp.first == true and bp.second == false ) {
        return 2u;
    }
    if ( bp.first == false and bp.second == true ) {
        return 2u;
    }
    if ( bp.first == false and bp.second == false ) {
        if ( i == j ) {
            return 0u;
        }
        else return 1u;
    }

    return 1u;
}


// print_matrix()
void Similarity::print_matrix() {

	const string x = m_seq1;
	const string y = m_seq2;

	vector<vector<size_t>> matrix = m_matrix;

    cout << "    ";
    for ( size_t k = 0; k <= y.size(); k++) cout << y[k] << ' ';
    cout << endl;
    size_t l = 0;
    while ( l < x.size()) {
        for ( size_t i = 0; i < matrix.size(); i++ ) {
            if ( i > 0 ) {
                cout << x[l] << ' ';
                l++;
            }
            for ( size_t j = 0; j < matrix[i].size(); j++ ) {
                if ( i == 0  and j  == 0) {
					cout << "  " << matrix[i][j] << " ";
                }
				else cout << matrix[i][j] << " ";
            }
            cout << endl;
        }
        cout << endl;
    }

}


// get_matrix()
vector<vector<size_t>> Similarity::get_matrix() {

	const string x = m_seq1;
    const string y = m_seq2;

    size_t m = x.size();
    size_t n = y.size();
   
    size_t sub = 1u, del = 1u, ins = 1u; 
    vector<vector<size_t>> matrix (m + 1u, vector<size_t> ( n + 1u )); 

    for( size_t i = 0u; i <= m; ++i ) matrix[i][0u] = i;
    for ( size_t j = 0u; j <= n; ++j ) matrix[0u][j] = j;
    sub = substitution(x[0], y[0]);
    for ( size_t i = 1u; i <= m; ++i ) { 
        for ( size_t j = 1u; j <= n; ++j ) { 
            sub = substitution(x[i-1u], y[j-1u]);
            if ( x[i-1u] == y[j-1u]) matrix[i][j] = matrix[i-1u][j-1u];
            else  matrix[i][j] = fmin(matrix[i-1u][j] + ins,
                                 fmin(matrix[i][j-1u] + del,
                                      matrix[i-1u][j-1u]) + sub);
        }   
    }   
	return matrix;
}

// compute_edit_distance()
size_t Similarity::compute_edit_distance () {

	const string x = m_seq1;
	const string y = m_seq2;

    size_t m = x.size();
    size_t n = y.size();
   
    size_t sub = 1u, del = 1u, ins = 1u;
    vector<vector<size_t>> matrix (m + 1u, vector<size_t> ( n + 1u ));
	
    for ( size_t i = 0u; i <= m; ++i ) matrix[i][0u] = i;
    for ( size_t j = 0u; j <= n; ++j ) matrix[0u][j] = j;
    //sub = substitution(x[0], y[0]);
    for ( size_t i = 1u; i <= m; ++i ) {
        for ( size_t j = 1u; j <= n; ++j ) {
			//sub = substitution(x[i-1u], y[j-1u]);
            if ( x[i-1u] == y[j-1u]) matrix[i][j] = matrix[i-1u][j-1u];
            else  matrix[i][j] = fmin(matrix[i-1u][j] + ins,
                                 fmin(matrix[i][j-1u] + del,
                                      matrix[i-1u][j-1u]) + sub);
        }
    }


    return matrix[m][n];
}

double Similarity::compute_median(size_t xi, size_t xj, size_t ed ) {
	
	double median = 0.0;
	double sqrted = sqrt(ed);
	double in = (xi*xj)/2;
	median = sqrted*in;
	return median;
}
	
double Similarity::inverse() {

	double sij = this->edit_distance;
	double inv = 1.0/(1.0 + sij);
	return inv;

}

double Similarity::angular_distance() {

	double inv = this->inverse();
	double ad = acos(inv);
	ad = ad/M_PI;
	ad = abs(ad);
	return ad;

}	

double Similarity::convert_to_degrees() {
	
	double ad = this->angular_distance();
	double deg = ad*(180.0/M_PI);
	return deg;

}


// soft cosine measure()
double Similarity::measure_soft_cosine() {


	//double sij = sim.edit_distance;

	vector<vector<size_t>> matrix = this->m_matrix;

	//const size_t N = fmax(x.size(),y.size());

	double soft_cosine = 0.0;
	double top = 0.0;
	double top_a = 0.0;
	double top_b = 0.0;
	double bottom_left = 0.0;
	double bottom_right = 0.0;
	
	/*for ( size_t k = 0; k < N; k++ ) {
		for ( size_t i = 0; i < matrix[k].size(); i++ ) {
			for ( size_t j = 0; j < matrix[i].size(); j++ ) {
				top_a += sim.compute_median(matrix[i][k], matrix[j][k], sij);
				top_b += sim.compute_median(matrix[k][i], matrix[k][j], sij);
				bottom_left += sqrt(sim.compute_median(matrix[i][k], matrix[j][k],sij));
				bottom_right += sqrt(sim.compute_median(matrix[k][i], matrix[k][j], sij));
			}
		}
	}*/	

	top = top_a*top_b;

	bottom_right = sqrt(bottom_right);
	bottom_left = sqrt(bottom_left);	

	double bottom = bottom_right*bottom_left;

	soft_cosine = top/bottom;
	return soft_cosine;
}


const string Similarity::get_hash_seq() {

	return this->m_seq1;

}
