CREATE SCHEMA genome_engine;

/* reference genomes*/
CREATE TABLE reference_genome(id INTEGER, 
							  name VARCHAR(128), 
							  date_added DATETIME);
/* reference gene */
CREATE TABLE reference_gene(id INTEGER, 
							reference_genome_id INTEGER, 
							chromosome INTEGER, 
							location_in_chromosome INTEGER,
							datastore_location VARCHAR(256),
							length INTEGER,
							hash VARCHAR(64));
/* gene_metadata */
CREATE TABLE gene_metadata(reference_gene_id INTEGER,
						   metadata_key VARCHAR(45),
						   metadata_value VARCHAR(45));
/* genome */
CREATE TABLE genome( id INTEGER,
					 date_ingested DATETIME);

/* gene */
CREATE TABLE gene(id INTEGER,
				  name VARCHAR(64),
				  genome_id INTEGER,
				  chromosome INTEGER,
				  location_in_chromosome INTEGER,
				  reference_gene INTEGER,
				  gene_delta TEXT,
				  length INTEGER,
				  hash VARCHAR(64));
