DROP SCHEMA IF EXISTS references;

CREATE SCHEMA references;
use references;

/* VariantSet - as seen in ga4gh/schemas/references.proto*/
CREATE TABLE Reference ( id VARCHAR(64),
						 length INTEGER,
						 md5checksum VARCHAR(64),
						 source_uri VARCHAR(64),
						 source_accessions VARCHAR(64),
					     is_dervied BOOLEAN,
						 source_divergence FLOAT,
						 ncbi_taxon_id INTEGER );

/* ReferenceSet - as seen in ga4gh/schemas/references.proto*/
CREATE TABLE ReferenceSet ( id VARCHAR(64),
							name VARCHAR(64),
							md5checksum VARCHAR(64),
							ncbi_taxon_id INTEGER,
							description VARCHAR(64),
							assembly_id VARCHAR(64),
							source_uri VARCHAR(64),
							source_accessions VARCHAR(64),
							is_derived BOOLEAN );
