# connector.py - Nicholas Hill
# Research and work done as part of SSRC/CRSS, Genome Data Engine
"""
This program connects to mysql database and inserts sequences with 
their md5 hash. The goal is to add the reference genome and populate the
deduplication library of the Genome Engine. 
"""
import mysql.connector
from mysql.connector import errorcode

import cython
import argparse
import os
import sys
import csv
import datetime
import string
import fileinput
import time
import ga4gh
import ga4gh.client as client
client =  client.HttpClient("http://1kgenomes.ga4gh.org")
import md5
import sqlalchemy
import multiprocessing 
# map of codons and their respective ascii values 
triplet_map = { "TTT": 35,
                "TTC": 43,
                "TTA": 45,
                "TTG": 46,
                "CTT": 1,
                "CTC": 19,
                "CTA": 18,
                "CTG": 17,
                "ATT": 16,
                "ATC": 5,
                "ATA": 7,
                "ATG": 2,
                "GTT": 57,
                "GTC": 53,
                "GTA": 51,
                "GTG": 55,
                "TCT": 32,
                "TCC": 47,
                "TCA": 49,
                "TCG": 44,
                "CCT": 9,
                "CCC": 26,
                "CCA": 24,
                "CCG": 22,
                "ACT": 10,
                "ACC": 48,
                "ACA": 3,
                "ACG": 4,
                "GCT": 29,
                "GCC": 52,
                "GCA": 50,
                "GCG": 54,
                "TAT": 27,
                "TAC": 42,
                "TAA": 40,
                "TAG": 38,
                "CAT": 14,
                "CAC": 20,
                "CAA": 25,
                "CAG": 31,
                "AAT": 15,
                "AAC": 6,
                "AAA": 21,
                "AAG": 12,
                "GAT": 33,
                "GAC": 60,
                "GAA": 63,
                "GAG": 56,
                "TGT": 29,
                "TGC": 36,
                "TGA": 59,
                "TGG": 62,
                "CGT": 61,
                "CGC": 64,
                "CGA": 30,
                "CGG": 34,
                "AGT": 23,
                "AGC": 11,
                "AGA": 13,
                "AGG": 8,
                "GGT": 28,
                "GGC": 41,
                "GGA": 39,
                "GGG": 37}
# MySQLAssistant class used for aiding in quick connections 
# and queries, for example ingesting the codon sequences 
class MySQLAssistant:

    def __init__(self):
        MySQLConfig = {'user':'root', 
                        'password':'Test123!',
                        'host':'localhost', 
                        'database':'hashdb'} 
        self.cnx = mysql.connector.connect(**MySQLConfig)
        self.cursor = self.cnx.cursor()

    def create_sub_hash_id_from_codon(self, codon):
        self.cursor.execute("SELECT seq_id FROM dedup WHERE seq = %s"% (codon))
        token = cursor.fetchone()
        print(token)
        return token

    def hash_in_codons(self):
        count = 0
        for k,j in triplet_map.iteritems():
            count += 1
            md5hash = md5.new(k).hexdigest()
            insert_hash = ("INSERT INTO dedup(hash,hash_id, seq, seq_id) VALUES(%s,%s,%s,%s)")
            hash_data = (md5hash,count, k,j)
            self.cursor.execute(insert_hash, hash_data)        
    
    def close_assistant(self):
        self.cnx.commit()
        self.cursor.close()
        self.cnx.close()    

# generates hashes for each codon and 
# returns a list of (hash, codon) pairs
def generate_hash_map(codon_list):

    iterator = 0
    for i in codon_list:
        md5hash = md5.new(i).hexdigest()
        codon_list[iterator] = (md5hash,i)
        iterator += 1

    return codon_list

# slides a window of k size throughout a sequence
# and then breaks the sequences into k sizes 
# and then returns a list of these sequences 
def slide_window(seq, k):

    codon_list = []
    i = 0
    while ( i < len(seq)):
        token = ""
        j = 0
        while ( j < k ):
            token += seq[i]
            i += 1
            j += 1
            if ( i == len(seq) ): break

        codon_list.append(token)

    return codon_list

# ingest the codon sequences
# and their associated ascii characters 
def initialize_db():

    mao = MySQLAssistant()
    cnx = mao.cnx
    mao.hash_in_codons()
    mao.close_assistant()

# returns true if an entry is already inside the hash_seq relation
def find(md5hash):

    mao = MySQLAssistant()
    cnx = mao.cnx
    cursor = mao.cursor
    query = ("SELECT hash FROM dedup WHERE hash = %s")
    data = (md5hash,)
    cursor.execute(query,data)
    row = cursor.fetchone()
    #print(row)
    truth = False
    if ( row > 0 ): truth = True 
    return truth 

# hash/ingest nonredundant data into the database 
def hash_into_database(begin, end, count):

    seq_list = obtain_reference_sequences(begin,end)

    try:
        cnx = mysql.connector.connect(user='root', password='Test123!',
                                    host='localhost', database='hashdb')
        cursor = cnx.cursor()
        # i = bases, j = start pos
        i = seq_list[0] 
        j = seq_list[1] 
        if ( len(i) > 3 ):
            codon_list = []
            codon_list = slide_window(i,3 )
            codon_map = []
            codon_map = generate_hash_map(codon_list)
            token = ''
            #for l,p in codon_map:
            #    query = ("SELECT seq_id FROM hash_seq WHERE seq = %s")
            #    req_seq = (p,)
            #    cursor.execute(query, req_seq)
            #    for x in cursor:
            #        token += (',' + x[0] )
            md5hash = md5.new(i).hexdigest()
            ref_count = count
            if ( find(md5hash) == True ): 
                ref_count += 1
                insert_reference = ("INSERT IGNORE INTO reference (chromosome, hash_id, seq_id, seq_start, seq_end) "
                                    "VALUES (%s,%s,%s,%s,%s) ")
                reference_data = (1, ref_count, token, j[0], j[1])
                cursor.execute(insert_reference, reference_data)
                cnx.commit()
            else: 
                count += 1
                ref_count += 1
                insert_hash = ("INSERT INTO dedup (hash, hash_id, seq,seq_id) VALUES(%s,%s,%s, %s)")
                hash_data = (md5hash, count, i, token)
                cursor.execute(insert_hash, hash_data)
                # insert concurrent data into the reference table
                insert_reference = ("INSERT INTO reference (chromosome, hash_id,seq_id, seq_start, seq_end) " 
                                    "VALUES (%s, %s, %s, %s, %s)")
                reference_data = (1, ref_count, token, j[0], j[1])
                cursor.execute(insert_reference, reference_data)
                cnx.commit()
        cnx.commit()
        cursor.close()
        cnx.close() 
    except mysql.connector.Error as err:
        if err.errno == errorcode.ER_ACCESS_DENIED_ERROR:
            print("Something is wrong with your user name or password")
        elif err.errno == errorcode.ER_BAD_DB_ERROR:
            print("Database does not exist")
        else:
            print(err)

# insert into the referenece table the hash_ids
def insert_ref_hash_ids():
    
    mao = MySQLAssistant()
    cnx = mao.cnx
    cursor = mao.cursor
    # insert hash_ids into reference
    insert_ref_hash_ids = ("INSERT INTO reference (hash_id) "
                           "SELECT dedup.hash_id FROM dedup "
                           "WHERE dedup.seq_id = seq_id")
                           # "JOIN reference r ON hash_seq.sub_hash_id = r.sub_hash_id")
    cursor.execute(insert_ref_hash_ids)
    mao.close_assistant()

# connect to 1000 genomes server and obtain reference sequences 
def obtain_reference_sequences(begin, end):
    
    reference_sets = client.search_reference_sets().next()
    #print (reference_sets)
    references = [r for r in client.search_references(reference_set_id=reference_sets.id)]
    #print(', '.join(sorted([reference.name for reference in references])))
    chr1 = filter(lambda x: x.name == "1", references)[0]
    bases = ((client.list_reference_bases(chr1.id, start=begin, end=end)), (begin, end))
    return bases

# main()
def main():

    start_time = time.time()
    initialize_db()
    
    begin = 1
    end = 97
    stop = 249250621
    #stop = 100000
    jobs = []
    print ( "inserting data...")
    count = 64
    while ( begin <= stop ):    
        p = multiprocessing.Process(target=hash_into_database, args=(begin,end,count))
        time.sleep(0.007)
        p.start()
        count += 1
        begin += 96
        end += 96

    print(">>> finished in %s seconds <<<" % (time.time() - start_time))

# run main()
if __name__ == "__main__":
    main()
                                               
