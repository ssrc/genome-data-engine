DROP SCHEMA IF EXISTS hashdb;

CREATE SCHEMA hashdb;
use hashdb;

CREATE TABLE hash_seq ( hash CHAR(32),
                        hash_id INTEGER PRIMARY KEY AUTO_INCREMENT, 
                        seq VARCHAR (100),
                        sub_hash_id VARCHAR (100) );
