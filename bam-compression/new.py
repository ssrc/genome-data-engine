import fileinput 
import pysam
import sys


samfile = pysam.AlignmentFile("exampleBAM.bam", "rb")

print "******************** Hello world! **************************"

#for read in samfile.fetch('chr1', 199, 255):
#    print read
    


for pileupcolumn in samfile.pileup("chr1", 199, 255):
    print ("\ncoverage at base %s = %s" % (pileupcolumn.pos, pileupcolumn.n))
    for pileupread in pileupcolumn.pileups:
        if not pileupread.is_del and not pileupread.is_refskip:
            print ('\tbase in read %s = %s' %
                  (pileupread.alignment.query_name,
                   pileupread.alignment.query_sequence[pileupread.query_position])) 




'''
with open(sys.argv[1], 'r') as input1:
    print(input1.read())

with open(sys.argv[2], 'r') as input2:
    print(input2.read())
'''


#samfile.close() 

