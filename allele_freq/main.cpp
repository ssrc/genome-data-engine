#include <utility>
#include <cstdlib>
#include <cstdio>
#include <fstream>
#include <iostream>
#include <sstream>
#include <iterator>
#include <vector>
#include <string>
#include <map>
#include <cmath>
#include <sstream>
#include <iomanip>
using namespace std;




void calculate_allele_frequency(string seq) {

	int A_count=0,C_count=0,G_count=0,T_count=0;
	char a = 'A';
	char c = 'C';
	char g = 'G';
	char t = 'T';
	double N = seq.size();
	for ( double i = 0.0; i < N; i++ ) {

		if ( seq[i] == a ) A_count++;
		if ( seq[i] == c ) C_count++;
		if ( seq[i] == g ) G_count++;
		if ( seq[i] == t ) T_count++;
	}

	
	double a_freq = (A_count/N)*100.0;
	double c_freq = (C_count/N)*100.0;
	double g_freq = (G_count/N)*100.0;
	double t_freq = (T_count/N)*100.0;

	cout << "Frequency of base A= " << a_freq << endl;
	cout << "Frequency of base C= " << c_freq << endl;
	cout << "Frequency of base G= " << g_freq << endl;
	cout << "Frequency of base T= " << t_freq << endl;

}



// main()
int main ( int argc, char** argv) {

    if ( argc != 2 ){  
        cout << "Usage: incompatible number of arguments." << endl;
        return EXIT_FAILURE;
    }   
    else if ( argc == 2){ 

		
		ifstream input(argv[1]);

		string file, hold;
		if ( input ) {
			while ( getline(input, hold)) file += hold;
		}

		calculate_allele_frequency(file);

	}
}
