#include <utility>
#include <cstdlib>
#include <cstdio>
#include <fstream>
#include <iostream>
#include <sstream>
#include <iterator>
#include <vector>
#include <string>
#include <map>
#include <cmath>
#include <sstream>
#include <iomanip>
using namespace std;

typedef map<string, size_t> G_Map;
typedef G_Map::iterator gmi;

typedef pair<char, int > base_pos_pair;
typedef vector<base_pos_pair> base_pos_vec;
typedef base_pos_vec::iterator base_pos_vec_iterator;

// check_if_purine()     
pair< bool, bool > check_if_purine( char i , char j ) {
    // A, G - purines
    // C, T - pyrimidines
    bool i_purine = true, j_purine = true;
 
    if ( i == 'A' or i == 'G' ) i_purine = true;
    else if ( i == 'C' or i == 'T' ) i_purine = false;
    if ( j == 'A' or j == 'G' ) j_purine = true;
    else if ( j == 'C' or j == 'T') j_purine = false;
    
    /*if ( i_purine == true and j_purine == true ) {
        cout << "both purines " << endl;
    }
    else if ( i_purine == false and j_purine == false ) {
        cout << "both pyrimidines " << endl;
    }
    else if ( i_purine == true and j_purine == false ) {
        cout << "one of each " << endl;
    }
    else if ( i_purine == false and j_purine == true ) {
        cout << "one of each" << endl;
    } */
    
    pair< bool, bool > boolean_pair = make_pair(i_purine, j_purine);

    return boolean_pair;
}

// Subsitution
size_t Substitution (char i , char j  ) {

    pair<bool, bool> bp = check_if_purine(i,j);

    if ( bp.first == true and bp.second == true ) {
        if ( i == j ) {
            return 0u;
        }
        else return 1u;
    }
    if ( bp.first == true and bp.second == false ) {
        return 2u;
    }
    if ( bp.first == false and bp.second == true ) {
        return 2u;
    }
    if ( bp.first == false and bp.second == false ) {
        if ( i == j ) {
            return 0u;
        }
        else return 1u;
    }

    return 1u;
}



// print_matrix()
void print_matrix(string x, string y, vector<vector<size_t>> matrix) {

    cout << "    ";
    for ( size_t k = 0; k <= y.size(); k++) cout << y[k] << ' ';
    cout << endl;
    size_t l = 0;
    while ( l < x.size()) {
        for ( size_t i = 0; i < matrix.size(); i++ ) {
            if ( i > 0 ) {
                cout << x[l] << ' ';
                l++;
            }
            for ( size_t j = 0; j < matrix[i].size(); j++ ) {
                if ( i == 0  and j  == 0) cout << "  " << matrix[i][j] << " ";
                else cout << matrix[i][j] << " ";
            }
            cout << endl;
        }
        cout << endl;
    }

}


// EditDistance()
size_t EditDistance ( string x, string y ) {
    
    size_t m = x.size();
    size_t n = y.size();
    
    size_t sub = 1u, del = 1u, ins = 1u;
    vector<vector<size_t>> matrix (m + 1u, vector<size_t> ( n + 1u ));
     
    for( size_t i = 0u; i <= m; ++i ) matrix[i][0u] = i;
    for ( size_t j = 0u; j <= n; ++j ) matrix[0u][j] = j;
    sub = Substitution(x[0], y[0]);
    for ( size_t i = 1u; i <= m; ++i ) {
        for ( size_t j = 1u; j <= n; ++j ) {
            sub = Substitution(x[i-1u], y[j-1u]);
            if ( x[i-1u] == y[j-1u]) matrix[i][j] = matrix[i-1u][j-1u];
            else  matrix[i][j] = fmin(matrix[i-1u][j] + ins,
                                 fmin(matrix[i][j-1u] + del,
                                      matrix[i-1u][j-1u]) + sub);
        }
    }

    print_matrix(x,y,matrix);

    return matrix[m][n];
} 


// slide_window()
G_Map slide_window(G_Map& lkup, base_pos_vec& v1, size_t k ) {
    base_pos_vec_iterator iter = v1.begin();
    base_pos_vec_iterator end = v1.end();
    while ( iter != end ) {
        string token;
        for ( size_t i = 0; i < k; i++ ) {
            if ( iter != end ) { 
                base_pos_pair value = *iter;
                token += value.first;
                token += value.second;
                iter++;
            }
        }
        G_Map::iterator p_entry = lkup.find(token);
        if ( p_entry == lkup.end() ) {   
            lkup.insert(pair<string, size_t>(token, 1u));
        }   
        else {
            p_entry->second++;    
        }   
        if ( next(iter, 2u) != end ) iter -= (k-1u);
        else break; 
    }     
    
    return lkup;

}

// print_G_map()
void print_G_map(G_Map map ) {

    gmi iter = map.begin();
    for(; iter != map.end(); iter++ ){
        cout << iter->first << " " << iter->second << endl;
    }
}



// call_window()
void call_window(base_pos_vec v1, base_pos_vec v2) {

    G_Map map1;
    G_Map map2;
    map1 = slide_window(map1, v1, 3u);
    map2 = slide_window(map2, v2, 3u);  
   
    size_t m1_total = 0u;
    gmi iter = map1.begin();
    for ( ; iter != map1.end(); iter++ ) {     
        m1_total += iter->second;
    }
    size_t m2_total = 0u;
    gmi iter2 = map2.begin();
    for ( ; iter2 != map2.end(); iter2++ ) {
        m2_total += iter2->second;
    }   

    cout << "total entries in file1 = " << m1_total << endl;;
    cout << "total entries in file2 = " << m2_total << endl;

    print_G_map(map1);
    cout << "***********************************************" << endl;
    print_G_map(map2); 

    /*cout << endl;
    cout << "Map1 size= " << map1.size() << endl;
    cout << "Map2 size= " << map2.size() << endl; 
    */
}


// load()
base_pos_vec load( base_pos_vec bp_vec, string sequence ) {
    char base;
    size_t base_pos = 1;
    pair<char, int> bp;
    for ( size_t pos = 0; pos <= sequence.size(); pos++ ) {
        base = sequence[pos];
        pair<char, int> bp (base, base_pos);
        bp_vec.push_back(bp);        
        base_pos++;
    }
    return bp_vec;
}

// print_sideby_side_vec()
void print_sidebyside_vec( base_pos_vec vec1, base_pos_vec vec2 ) {
    base_pos_vec_iterator iter1 = vec1.begin();
    base_pos_vec_iterator iter2 = vec2.begin();
    for(; iter1 != vec1.end() and iter2 != vec2.end(); iter1++, iter2++) {
        cout << iter1->first << " " << iter1->second << " \t " << 
        iter2->first << " " << iter2->second << endl;
        if ( next(iter1,1) == vec1.end() and 
             next(iter2,1) == vec2.end()) break;
    }
}


// compare_vec()
size_t compare_vec(base_pos_vec vec1, base_pos_vec vec2 ) {

    base_pos_vec_iterator iter1 = vec1.begin();
    base_pos_vec_iterator iter2 = vec2.begin();
    char base1;
    char base2;
    size_t pos1;
    size_t pos2;
    int similar = 0;
    for(; iter1 != vec1.end() and iter2 != vec2.end(); iter1++, iter2++) {
        base1 = iter1->first;
        base2 = iter2->first;    
        pos1 = iter1->second;
        pos2 = iter2->second;
        if ( base1 == base2 and pos1 == pos2) {
            similar++;
        }
    }
    return similar;
}

// percent_identity()
double percent_identity( double identical, double n ) {

    double pi = 0.0;
    //cout << identical;
    pi = (identical/n)*100.0;    
    if ( pi > 100.0 ) pi = 100.0;

    return pi;
}


// main()
int main ( int argc, char** argv) {

    if ( argc != 3 ){ 
        cout << "Usage: incompatible number of arguments." << endl;
        return EXIT_FAILURE;
    }   
    else if ( argc == 3){
    
        ifstream input1 ( argv[1] );
        ifstream input2 ( argv[2] );
        
        if ( input1 and input2 ) {
           
            // place files in strings and eliminate whitespace
            string file1, file2, hold1, hold2; 
            while ( getline(input1, hold1 )) file1 += hold1;
            while ( getline(input2, hold2)) file2 += hold2;
          
            // get files character lengths
            double file1_size = file1.size();
            double file2_size = file2.size();
            cout << "The first file is " << file1_size << " characters" << endl;
            cout << "The second file is " << file2_size << " characters" << endl;

            base_pos_vec seq1;
            base_pos_vec seq2;
 
            seq1 = load( seq1, file1 );
            seq2 = load( seq2, file2 );
            print_sidebyside_vec(seq1, seq2);
            
            //
            //  print out position specific percent identity
            //
            /*double identical_count = compare_vec(seq1, seq2);
            cout << identical_count << endl;;
            double pi = percent_identity(identical_count, file1_size );
            cout << "The sequences are " << pi << "% identitcal" << endl;
            */

            //call_window(seq1, seq2);
            
            size_t value = EditDistance(file1, file2);
            cout <<"*****" << value <<"*********" <<endl;
            
            input1.close();
            input2.close();

        }
        
    } 

    return EXIT_SUCCESS;
}
