# /usr/bin/env python3
# genome_engine_dao.py - Trevor Pesout
# Research and work done as part of SSRC/CRSS, Genome Data Engine

from ..referenceSeeding.helper_functions import *
import mysql.connector
import mysql.connector.cursor as mysql_cursors
import db.db_config as config

class GenomeEngineDAO(object):

    def __init__(self, auto_commit=True):
        self.connection = None
        self.auto_commit = auto_commit

    def insert_known_gene(self, gene: 'Gene', auto_commit=None):
        def intlist2str(intlist):
            if intlist is None or len(intlist) == 0: return ""
            else: return "," + ",".join(map(str, intlist)) + ","

        #prep the data
        sql = "INSERT INTO known_gene "\
                "(name, chromosome, positive_strand, tx_start, tx_end, cds_start, cds_end," \
                    "exon_count, exon_starts, exon_ends, protein_id, align_id)" \
              "VALUES (%s, %s, %s, %s, %s, %s, %s, " \
                    "%s, %s, %s, %s, %s)"
        gene_data = (gene.name, gene.chrom, gene.positive_strand, gene.tx_start, gene.tx_end, gene.cds_start, gene.cds_end,
                     gene.exon_count, intlist2str(gene.exon_starts), intlist2str(gene.exon_ends), gene.protein_id, gene.align_id)

        #execute the command
        cursor = self.get_cursor()
        cursor.execute(sql, gene_data)

        #return generated id
        id = cursor.lastrowid
        cursor.close()
        self._commit(auto_commit)

        return id

    def insert_reference_gene(self, genome_id, name, chromosome, positive_strand,
                              start_position, end_position, datastore_location=None,
                              sequence=None, auto_commit=None):

        #prep the data
        sql = "INSERT INTO reference_gene "\
                "(reference_genome_id, reference_gene_name, reference_gene_chromosome, reference_gene_positive_strand, " \
                "reference_gene_start_position, reference_gene_end_position, reference_gene_datastore_location)" \
              "VALUES (%s, %s, %s, %s, %s, %s, %s)"
        gene_data = (genome_id, name, chromosome, positive_strand, start_position, end_position, datastore_location)

        #execute the command
        cursor = self.get_cursor()
        cursor.execute(sql, gene_data)

        #get generated id
        reference_gene_id = cursor.lastrowid

        #add sequence (if appropriate)
        if sequence is not None:
            sql = "INSERT INTO reference_gene_sequence " \
                  "(reference_gene_id, reference_gene_sequence)" \
                  "VALUES (%s, %s)"
            cursor.execute(sql, (reference_gene_id, sequence))

        cursor.close()
        self._commit(auto_commit)

        return reference_gene_id

    def get_known_gene(self, gene_id = None, gene_name = None):
        #sanity check
        if gene_id is None and gene_name is None:
            raise Exception("GenomeEngineDAO.get_known_gene needs either gene_id or gene_name set")

        # sql data
        sql = "SELECT * FROM known_gene_gb WHERE "
        if gene_id is None:
            sql += "name = %(gene_name)s"
        if gene_name is None:
            sql += "id = %(gene_id)s"
        else:
            sql += "name = %(gene_name)s AND id = %(gene_id)s"

        #execute query
        cursor = self.get_cursor()
        cursor.execute(sql, {'gene_name':gene_name, 'gene_id':gene_id})

        # get result
        result = None
        for row in cursor:
            if result != None:
                print_warn("Found %d results getting gene %s/%s" % (len(cursor), str(gene_id), gene_name), "GE_DAO")
            result = row

        cursor.close()

        return result

    def insert_reference_chunk(self, chunk: 'GeneChunk', auto_commit=None):

        # prep the data
        sql = "INSERT INTO reference_chunk " \
                  "(reference_genome_id, reference_chunk_gene_count, reference_chunk_chromosome, " \
                  "reference_chunk_positive_strand, reference_chunk_start_position, reference_chunk_end_position)"\
              "VALUES (%s, %s, %s, %s, %s, %s)"
        gene_data = (chunk.genome_id, chunk.gene_count, chunk.chromosome,
                     chunk.positive_strand, chunk.start_position, chunk.end_position)

        # execute the command
        cursor = self.get_cursor()
        cursor.execute(sql, gene_data)

        # return generated id
        id = cursor.lastrowid
        cursor.close()
        self._commit(auto_commit)

        return id



    def test(self):

        try:
            self._connect()
            if self.connection is not None:
                cursor = self.get_cursor()
                cursor.execute("SHOW TABLES;")
                for row in cursor:
                    print(row)
                cursor.close()
            else:
                print_err("Could not connect to GenomeEngine database!", "GE_DAO")
        finally:
            self.close()


    def get_cursor(self):
        return self.connection.cursor(cursor_class=mysql_cursors.MySQLCursorDict)

    def connect(self, cfg = None):
        self._connect(cfg)
        if self.connection == None:
            raise Exception("Could not connect to GenomeEngine database")

    def _connect(self, cfg = None):
        # see if already connected
        if self.connection != None and self.connection.is_connected():
            return self.connection

        # get default config
        if cfg is None:
            cfg = config.get_genome_engine_db_config()

        # connect
        try:
            self.connection = mysql.connector.connect(**cfg)
        except Exception as e:
            print(e, file=sys.stderr)
            self.connection = None

        # return connection or None
        return self.connection

    def rollback(self):
        self.connection.rollback()

    def commit(self):
        self.connection.commit()

    def _commit(self, do_commit = None):
        if (do_commit is None and self.auto_commit) or do_commit:
            self.commit()

    def close(self):
        if self.connection != None:
            try:
                self.connection.close()
            except:
                pass;

    def __enter__(self):
        self.connect()
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        self.close()

    def __del__(self):
        self.close()


if __name__ == "__main__":
    with GenomeEngineDAO() as dao:
        dao.test()
                                               
