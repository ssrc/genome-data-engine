-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

-- -----------------------------------------------------
-- Schema ingestion
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Schema ingestion
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `ingestion` DEFAULT CHARACTER SET utf8 ;
USE `ingestion` ;

-- -----------------------------------------------------
-- Table `ingestion`.`ingestion_job`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `ingestion`.`ingestion_job` (
  `ingestion_job_id` INT NOT NULL AUTO_INCREMENT,
  `ingestion_job_bam_location` VARCHAR(256) NULL,
  `ingestion_job_bam_location_type` VARCHAR(16) NULL,
  `ingestion_job_chrom_location` VARCHAR(256) NULL,
  `ingestion_job_chrom_location_type` VARCHAR(16) NULL,
  `ingestion_job_state` VARCHAR(16) NOT NULL,
  `ingestion_job_date_created` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `ingestion_job_date_updated` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`ingestion_job_id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `ingestion`.`ingestion_chrom_job`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `ingestion`.`ingestion_chrom_job` (
  `ingestion_chrom_job_id` INT NOT NULL AUTO_INCREMENT,
  `ingestion_job_id` INT NOT NULL,
  `ingestion_chrom_job_chromosome` VARCHAR(64) NOT NULL,
  `ingestion_chrom_job_state` VARCHAR(16) NOT NULL,
  `ingestion_chrom_job_date_created` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `ingestion_chrom_job_date_updated` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`ingestion_chrom_job_id`, `ingestion_job_id`),
  INDEX `fk_ingestion_chromosome_job_ingestion_job_idx` (`ingestion_job_id` ASC),
  CONSTRAINT `fk_ingestion_chromosome_job_ingestion_job`
    FOREIGN KEY (`ingestion_job_id`)
    REFERENCES `ingestion`.`ingestion_job` (`ingestion_job_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `ingestion`.`ingestion_seq_job`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `ingestion`.`ingestion_seq_job` (
  `ingestion_seq_job_id` INT NOT NULL AUTO_INCREMENT,
  `ingestion_chrom_job_id` INT NOT NULL,
  `ingestion_job_id` INT NOT NULL,
  `ingestion_seq_job_start_pos` INT NOT NULL,
  `ingestion_seq_job_end_pos` INT NOT NULL,
  `ingestion_seq_job_reference_gene_id` INT NULL,
  `ingestion_seq_job_date_created` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `ingestion_seq_job_date_updated` DATETIME NOT NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`ingestion_seq_job_id`, `ingestion_chrom_job_id`, `ingestion_job_id`),
  INDEX `fk_ingestion_seq_job_ingestion_chrom_job1_idx` (`ingestion_chrom_job_id` ASC, `ingestion_job_id` ASC),
  CONSTRAINT `fk_ingestion_seq_job_ingestion_chrom_job1`
    FOREIGN KEY (`ingestion_chrom_job_id` , `ingestion_job_id`)
    REFERENCES `ingestion`.`ingestion_chrom_job` (`ingestion_chrom_job_id` , `ingestion_job_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
