import sys
import datetime

def print_err(msg, module_identifier =""):
    print_log_message("ERROR", module_identifier, msg)

def print_warn(msg, location = ""):
    print_log_message("WARN", location, msg)

def print_info(msg, location = ""):
    print_log_message("INFO", location, msg)

def print_log_message(level, location, msg):
    print("%5s : %s : %8s : %s" % (level, datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S"), location, msg), file=sys.stderr)