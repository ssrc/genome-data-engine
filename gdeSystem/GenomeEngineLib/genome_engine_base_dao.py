# /usr/bin/env python3
# genome_engine_base_dao.py - Trevor Pesout
# Research and work done as part of SSRC/CRSS, Genome Data Engine

from GenomeEngineLib.helper_functions import *
import mysql.connector
import mysql.connector.cursor as mysql_cursors

class GenomeEngineBaseDAO(object):

    def __init__(self, auto_commit=True):
        self.connection = None
        self.auto_commit = auto_commit


    ### OVERRIDE THESE ###

    def get_dao_identifier(self):
        return "BASE_DAO"

    def get_default_connection_parameters(self):
        return None


    ### NOT INTENDED TO BE OVERWRITTEN ###

    def test(self):
        try:
            self.connect()
            if self.connection is not None:
                cursor = self.get_cursor()
                cursor.execute("SHOW TABLES;")
                for row in cursor:
                    print(row)
                cursor.close()
            else:
                print_err("Could not connect to GenomeEngine database!", "GE_DAO")
        finally:
            self.close()


    def get_cursor(self):
        return self.connection.cursor(cursor_class=mysql_cursors.MySQLCursorDict)

    def connect(self, cfg = None, silent_fail=False):
        # see if already connected
        if self.connection != None and self.connection.is_connected():
            return self.connection

        # get default configuration
        if cfg == None:
            cfg = self.get_default_connection_parameters()

        # get default config
        if cfg is None:
            print_err(msg="No default connection parameters specified!", module_identifier=self.get_dao_identifier())
            if not silent_fail:
                raise Exception("No default connection parameters specified on %s" % self.get_dao_identifier())

        # connect
        try:
            self.connection = mysql.connector.connect(**cfg)
        except Exception as e:
            print_err(msg=str(e), module_identifier=self.get_dao_identifier())
            self.connection = None
            if not silent_fail:
                raise e

        # return connection or None
        return self.connection

    def rollback(self):
        self.connection.rollback()

    def commit(self):
        self.connection.commit()

    def _commit(self, do_commit = None):
        if (do_commit is None and self.auto_commit) or do_commit:
            self.commit()

    def close(self):
        if self.connection != None:
            try:
                self.connection.close()
            except:
                pass;

    def __enter__(self):
        self.connect()
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        self.close()

    def __del__(self):
        self.close()



