#!/usr/bin/env python3

from ..GenomeEngineLib.helper_functions import *
import random
import time
from genome_engine_dao import *
from gene_objects import *


class GenomeDatabaseSeedUtility:
    DEBUG = False
    MAX_CHUNK_SIZE = 2**20 #around one million bases

    HG38_REFERENCE_GENOME_ID = 1
    ALL_CHROMOSOMES = ("chr1", "chr2", "chr3", "chr4", "chr5", "chr6", "chr7", "chr8", "chr9", "chr10", "chr11", "chr12",
        "chr13", "chr14", "chr15", "chr16", "chr17", "chr18", "chr19", "chr20", "chr21", "chr22", "chrM", "chrX", "chrY")

    def __init__(self, current_chromosome = "chr1"):
        self.current_chromosome = current_chromosome
        self.current_sequence = None
        self.current_known_genes = None

    def main(self, chromosomes = None):
        if chromosomes == None:
            chromosomes = GenomeDatabaseSeedUtility.ALL_CHROMOSOMES
        if isinstance(chromosomes, str):
            chromosomes = [chromosomes]


        print_info("Importing chromosomes: %s" % str(chromosomes))
        start = time.time()
        success = False

        try:
            for chromosome in chromosomes:
                self.do_chromosome_import(chromosome)
            success = True

        finally:
            end = time.time()
            total = end - start
            time_str = "%d hours, %d minutes, %d seconds" % (int(total / 3600), int((total % 3600) / 60), int(total % 60))
            if success:
                print_info("Import succeeded after " + time_str, "MAIN")
            else:
                print_err("Import failed after " + time_str, "MAIN")
            print_info("Fin.")

    def do_chromosome_import(self, chromosome):
        print_info("Starting %s" % chromosome, "MAIN")
        start = time.time()

        self.set_current_chromosome(chromosome)
        self.import_known_genes()
        self.determine_overlap()

        end = time.time()
        total = end - start
        print_info("Finished %s (%d minutes, %d seconds)." % (chromosome, int(total / 60), int(total % 60)), "MAIN")

    def import_known_genes(self, chromosome_name="chr1"):
        chromosome_sequence = self.get_chromosome_sequence()
        gene_set = self.get_known_genes()
        print_info("Importing %d known genes" % len(gene_set), "SEEDER")
        start = time.time()
        with GenomeEngineDAO(auto_commit=True) as dao:
            max_failure = 1
            count = 0
            next_report = int(len(gene_set) / 8 ) + 1
            for gene in gene_set:
                try:
                    known_gene_id = dao.insert_known_gene(
                        gene,
                        auto_commit=False)
                    sequence = chromosome_sequence.get_subsequence(
                        gene.tx_start,
                        gene.tx_end)
                    ref_id = dao.insert_reference_gene(
                        genome_id=GenomeDatabaseSeedUtility.HG38_REFERENCE_GENOME_ID,
                        name=gene.name,
                        chromosome=gene.chrom,
                        positive_strand=gene.positive_strand,
                        start_position=gene.tx_start,
                        end_position=gene.tx_end,
                        datastore_location=None,
                        sequence=sequence,
                        auto_commit=True)
                    count += 1
                    if count == next_report:
                        print_info("Imported %d / %d genes" % (count, len(gene_set)), "SEEDER")
                        next_report *= 2
                except Exception as e:
                    print_err("Failed to ingest gene %s:\n\t%s\n\t%s" % (str(gene.name), str(gene), str(e)), "SEEDER")
                    dao.rollback()
                    max_failure -= 1
                    if (max_failure == 0):
                        print_err("Maximum number of acceptable falures occurred", "SEEDER")
                        raise e
        print_info("Imported %d genes in %d seconds" % (len(gene_set), int(time.time() - start)), "SEEDER")

    def get_sequences_by_name(self, genes: 'GeneSet', sequence: 'Sequence'):
        print('>', end="", flush=True)
        for line in sys.stdin:
            gene_name = line.strip()
            if gene_name.upper() == "QUIT" or gene_name.upper() == "EXIT": break
            try:
                gene = genes.get_gene(gene_name)
                if gene == None:
                    print_err("Could not find gene '%s'" % (gene_name), "SEEDER")
                else:
                    print(gene)
                    seq = sequence.get_subsequence(gene.tx_start, gene.tx_end)
                    print(seq)
            except Exception as e:
                print(e)

            print('>', end="", flush=True)



    def determine_overlap(self):
        # get list of genes
        gene_set = self.get_known_genes()
        gene_list = gene_set.gene_list

        # for reporting
        print_info("Determining overlap for %d genes" % len(gene_set), "OVERLAP")
        start = time.time()

        # convert to a more malleable format
        bgene_list = list(map(
            lambda gene: {
                "id":[gene.name],
                "start":gene.tx_start,
                "end":gene.tx_end,
                "pos":gene.positive_strand
            },
            gene_list
        ))

        # filter into positive and negative
        positive_genes = list(filter(lambda x: x["pos"], bgene_list))
        negative_genes = list(filter(lambda x: not x["pos"], bgene_list))

        # build gene overlap data
        self.build_overlap(positive_genes)
        self.build_overlap(negative_genes)

        # get all gene chunks (including matched gene chunks)
        pos_chunks = self.get_gene_chunks(positive_genes)
        neg_chunks = self.get_gene_chunks(negative_genes)

        # combine pos an neg
        all_chunks =  pos_chunks + neg_chunks

        # save to database
        self.save_gene_chunks(all_chunks)

        # reporting
        print_info("Imported %d reference chunks in %d seconds" % (len(all_chunks), int(time.time() - start)), "SEEDER")

    def build_overlap(self, gene_list):
        # sort the gene list
        gene_list.sort(key=(lambda x: x["start"]))

        # for reporting and data mgmnt
        original_len = len(gene_list)
        combined = 0
        current_gene = None
        genes_to_remove = list()

        # iterate over genes
        for gene in gene_list:
            # we update current gene with ids and end positions
            # until we encounter a gene which starts after our current end
            # and then remove the "absorbed" genes
            if current_gene is None:
                current_gene = gene
            elif current_gene["end"] >= gene["start"]:
                for id in gene["id"]:
                    current_gene["id"].append(id)
                current_gene["end"] = max(current_gene["end"], gene["end"])
                genes_to_remove.append(gene)
                combined += 1
            else:
                current_gene = gene

        # remove absorbed
        for gene in genes_to_remove:
            gene_list.remove(gene)

        # reporting
        combined_len = len(gene_list)
        print_info("Combined %d genes into %d overlapping chunks" % (original_len, combined_len), "OVERLAP")

        return combined


    def get_gene_chunks(self, genes):
        # verify data type still map{"id", "start", "end", "pos"}
        if not isinstance(genes[0], dict):
            raise Exception("GenomeDatabaseSeedUtility.get_gene_chunks needs list of dicts as parameter")

        # required variables
        unmapped_sequences = list()
        current_idx = 0
        pos = genes[0]["pos"]

        # for appending gene chunks which may be too big
        def get_gene_chunks(start_idx, end_idx):
            total_chunk_size = end_idx - start_idx
            total_chunk_count = int(total_chunk_size / GenomeDatabaseSeedUtility.MAX_CHUNK_SIZE) + 1
            each_chunk_size = int(total_chunk_size / total_chunk_count) + 1

            # reporting
            if GenomeDatabaseSeedUtility.DEBUG and total_chunk_count > 1:
                print_info("Splitting chunk from %d to %d into %d pieces of size %d" %
                           (current_idx, end_idx, total_chunk_count, each_chunk_size), "OVERLAP")

            # append one or more chunks
            chunk_list = list()
            for i in range(0, total_chunk_count):
                s = current_idx + i * each_chunk_size
                e = s + each_chunk_size - 1
                if e > end_idx: e = end_idx
                chunk_list.append({
                    "id": None,
                    "start": s,
                    "end": e,
                    "pos": pos
                })
            return chunk_list

        # iterate over genes, tracking where the last gene ended
        for gene in genes:
            # get start of this gene
            gene_start = gene["start"]

            # case where genes line up exactly (mostly shouldn't happen)
            if current_idx >= gene_start - 1:
                print_warn("Encountered gene %s with start around current_idx %d"
                           % (str(gene), current_idx), "OVERLAP")
                continue

            # this is the gap between last gene and this one
            end_idx = gene_start - 1

            # to avoid having gene sequences which are too long we break
            # it up into chunks around the size of MAX_CHUNK_SIZE
            for chunk in get_gene_chunks(current_idx, end_idx):
                unmapped_sequences.append(chunk)

            # set the start position of the next "gap"
            current_idx = gene["end"] + 1

        # append final chunks (need the end index of chromosome for this)
        chromosome_sequence = self.get_chromosome_sequence()
        for chunk in get_gene_chunks(current_idx, chromosome_sequence.max_index):
            unmapped_sequences.append(chunk)

        # combine the real genes and the unmapped chunks, sort
        all_chunks = genes + unmapped_sequences
        all_chunks.sort(key=(lambda x: x["start"]))

        # create GeneChunk objects from these
        chunks = list(map(
            lambda gene:
                GeneChunk(
                    genome_id=GenomeDatabaseSeedUtility.HG38_REFERENCE_GENOME_ID,
                    gene_count=0 if gene["id"] is None else len(gene["id"]),
                    chromosome=self.current_chromosome,
                    positive_strand=gene["pos"],
                    start_position=gene["start"],
                    end_position=gene["end"]
                ),
            all_chunks
        ))

        # print 'em if appropriate
        if GenomeDatabaseSeedUtility.DEBUG:
            for chunk in chunks:
                print_info(str(chunk), "OVERLAP")

        # return
        return chunks


    def save_gene_chunks(self, chunks):
        sequence = self.get_chromosome_sequence()
        print_info("Importing %d reference chunks" % len(chunks), "OVERLAP")
        with GenomeEngineDAO() as dao:
            max_failure = 1
            count = 0
            next_report = int(len(chunks) / 8) + 1
            for chunk in chunks:
                try:
                    if chunk.gene_count == 0:
                        dao.insert_reference_gene(
                            genome_id=chunk.genome_id, name=None, chromosome=chunk.chromosome,
                            positive_strand=chunk.positive_strand, start_position=chunk.start_position,
                            end_position=chunk.end_position, datastore_location=None,
                            sequence=sequence.get_subsequence(chunk.start_position, chunk.end_position),
                            auto_commit=False)

                    dao.insert_reference_chunk(chunk, auto_commit=True)
                    count += 1
                    if count == next_report:
                        print_info("Imported %d / %d chunks" % (count, len(chunks)), "OVERLAP")
                        next_report *= 2
                except Exception as e:
                    print_err("Failed to save GeneChunk:\n\t%s\n\t%s" % (str(chunk), str(e)), "OVERLAP")
                    dao.rollback()
                    max_failure -= 1
                    if (max_failure == 0):
                        print_err("Maximum number of acceptable failures occurred", "OVERLAP")
                        raise e



    def set_current_chromosome(self, chromosome):
        if self.current_chromosome != chromosome:
            self.current_sequence = None
            self.current_known_genes = None
        self.current_chromosome = chromosome

    def get_chromosome_sequence(self):
        # return if already saved
        if self.current_sequence is not None:
            return self.current_sequence
        # sanity check
        if self.current_chromosome == None:
            raise Exception("Current Chromosome is Null!")

        # read file, save, and return
        filename = "sequences/%s.fa" % self.current_chromosome
        self.current_sequence = Sequence().import_sequence_file(filename)
        return self.current_sequence

    def get_known_genes(self):
        # return if already saved
        if self.current_known_genes is not None:
            return self.current_known_genes
        # sanity check
        if self.current_chromosome == None:
            raise Exception("Current Chromosome is Null!")

        # read file, save, and return
        filename = "genes/knownGenes_%s.tsv" % self.current_chromosome
        self.current_known_genes = GeneSet().import_tsv_file(filename)
        return self.current_known_genes

if __name__ == "__main__":
    GenomeDatabaseSeedUtility().main()