-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

-- -----------------------------------------------------
-- Schema mydb
-- -----------------------------------------------------
-- -----------------------------------------------------
-- Schema genome_engine
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Schema genome_engine
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `genome_engine` DEFAULT CHARACTER SET utf8 COLLATE utf8_bin ;
USE `genome_engine` ;

-- -----------------------------------------------------
-- Table `genome_engine`.`ingested_genome`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `genome_engine`.`ingested_genome` (
  `ingested_genome_id` INT(11) NOT NULL AUTO_INCREMENT,
  `ingested_genome_name` VARCHAR(128) NULL DEFAULT NULL,
  `date_ingested` DATETIME NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`ingested_genome_id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `genome_engine`.`reference_genome`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `genome_engine`.`reference_genome` (
  `reference_genome_id` INT(11) NOT NULL AUTO_INCREMENT,
  `reference_genome_name` VARCHAR(128) NOT NULL,
  `reference_genome_ingested_id` INT(11) NULL DEFAULT NULL,
  `reference_genome_date_added` DATETIME NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`reference_genome_id`))
ENGINE = InnoDB
AUTO_INCREMENT = 2
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `genome_engine`.`reference_gene`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `genome_engine`.`reference_gene` (
  `reference_gene_id` INT(11) NOT NULL AUTO_INCREMENT,
  `reference_genome_id` INT(11) NOT NULL,
  `reference_gene_name` VARCHAR(128) NULL DEFAULT NULL,
  `reference_gene_chromosome` VARCHAR(128) NOT NULL,
  `reference_gene_positive_strand` TINYINT(1) NOT NULL,
  `reference_gene_start_position` INT(11) NOT NULL,
  `reference_gene_end_position` INT(11) NOT NULL,
  `reference_gene_datastore_location` VARCHAR(256) NULL DEFAULT NULL,
  `reference_gene_date_added` DATETIME NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`reference_gene_id`, `reference_genome_id`),
  INDEX `fk_reference_gene_reference_genome_idx` (`reference_genome_id` ASC),
  UNIQUE INDEX `reference_gene_name_UNIQUE` (`reference_gene_name` ASC),
  CONSTRAINT `fk_reference_gene_reference_genome`
    FOREIGN KEY (`reference_genome_id`)
    REFERENCES `genome_engine`.`reference_genome` (`reference_genome_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `genome_engine`.`ingested_chunk`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `genome_engine`.`ingested_chunk` (
  `ingested_chunk_id` INT(11) NOT NULL AUTO_INCREMENT,
  `ingested_genome_id` INT(11) NOT NULL,
  `reference_gene_id` INT(11) NOT NULL,
  `ingested_chunk_name` VARCHAR(64) NULL DEFAULT NULL,
  `ingested_chunk_chromosome` VARCHAR(128) NOT NULL,
  `ingested_chunk_positive_strand` TINYINT(1) NOT NULL,
  `ingested_chunk_start_position` INT(11) NOT NULL,
  `ingested_chunk_end_position` INT(11) NOT NULL,
  `reference_chunk_id` INT(11) NULL DEFAULT NULL,
  PRIMARY KEY (`ingested_chunk_id`, `ingested_genome_id`),
  INDEX `fk_ingested_chunk_ingested_genome1_idx` (`ingested_genome_id` ASC),
  INDEX `fk_ingested_chunk_reference_gene1_idx` (`reference_gene_id` ASC),
  CONSTRAINT `fk_ingested_chunk_ingested_genome1`
    FOREIGN KEY (`ingested_genome_id`)
    REFERENCES `genome_engine`.`ingested_genome` (`ingested_genome_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_ingested_chunk_reference_gene1`
    FOREIGN KEY (`reference_gene_id`)
    REFERENCES `genome_engine`.`reference_gene` (`reference_gene_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `genome_engine`.`ingested_chunk_metadata`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `genome_engine`.`ingested_chunk_metadata` (
  `ingested_chunk_metadata_id` INT(11) NOT NULL AUTO_INCREMENT,
  `ingested_chunk_id` INT(11) NOT NULL,
  `ingested_chunk_metadata_key` VARCHAR(64) NULL DEFAULT NULL,
  `ingested_chunk_metadata_value` VARCHAR(256) NULL DEFAULT NULL,
  PRIMARY KEY (`ingested_chunk_metadata_id`, `ingested_chunk_id`),
  INDEX `fk_ingested_chunk_metadata_ingested_chunk1_idx` (`ingested_chunk_id` ASC),
  CONSTRAINT `fk_ingested_chunk_metadata_ingested_chunk1`
    FOREIGN KEY (`ingested_chunk_id`)
    REFERENCES `genome_engine`.`ingested_chunk` (`ingested_chunk_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `genome_engine`.`ingested_chunk_sequence`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `genome_engine`.`ingested_chunk_sequence` (
  `ingested_chunk_id` INT(11) NOT NULL,
  `ingested_chunk_sequence` LONGTEXT NOT NULL,
  INDEX `fk_ingested_chunk_sequence_ingested_chunk1_idx` (`ingested_chunk_id` ASC),
  UNIQUE INDEX `ingested_chunk_id_UNIQUE` (`ingested_chunk_id` ASC),
  CONSTRAINT `fk_ingested_chunk_sequence_ingested_chunk1`
    FOREIGN KEY (`ingested_chunk_id`)
    REFERENCES `genome_engine`.`ingested_chunk` (`ingested_chunk_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `genome_engine`.`known_gene`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `genome_engine`.`known_gene` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(128) NULL DEFAULT NULL,
  `chromosome` VARCHAR(128) NULL DEFAULT NULL,
  `positive_strand` TINYINT(1) NULL DEFAULT NULL,
  `tx_start` INT(11) NULL DEFAULT NULL,
  `tx_end` INT(11) NULL DEFAULT NULL,
  `cds_start` INT(11) NULL DEFAULT NULL,
  `cds_end` INT(11) NULL DEFAULT NULL,
  `exon_count` INT(11) NULL DEFAULT NULL,
  `exon_starts` TEXT NULL DEFAULT NULL,
  `exon_ends` TEXT NULL DEFAULT NULL,
  `protein_id` VARCHAR(128) NULL DEFAULT NULL,
  `align_id` VARCHAR(128) NULL DEFAULT NULL,
  `date_added` DATETIME NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `name_UNIQUE` (`name` ASC))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `genome_engine`.`reference_chunk`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `genome_engine`.`reference_chunk` (
  `reference_chunk_id` INT(11) NOT NULL AUTO_INCREMENT,
  `reference_genome_id` INT(11) NOT NULL,
  `reference_chunk_gene_count` INT(11) NOT NULL,
  `reference_chunk_chromosome` VARCHAR(128) NOT NULL,
  `reference_chunk_positive_strand` TINYINT(1) NOT NULL,
  `reference_chunk_start_position` INT(11) NOT NULL,
  `reference_chunk_end_position` INT(11) NOT NULL,
  `reference_chunk_date_added` DATETIME NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`reference_chunk_id`, `reference_genome_id`),
  INDEX `fk_reference_chunk_reference_genome1_idx` (`reference_genome_id` ASC),
  CONSTRAINT `fk_reference_chunk_reference_genome1`
    FOREIGN KEY (`reference_genome_id`)
    REFERENCES `genome_engine`.`reference_genome` (`reference_genome_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `genome_engine`.`reference_gene_metadata`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `genome_engine`.`reference_gene_metadata` (
  `reference_gene_metadata_id` INT(11) NOT NULL AUTO_INCREMENT,
  `reference_gene_id` INT(11) NOT NULL,
  `reference_gene_metadata_key` VARCHAR(64) NULL DEFAULT NULL,
  `reference_gene_metadata_value` VARCHAR(256) NULL DEFAULT NULL,
  PRIMARY KEY (`reference_gene_metadata_id`, `reference_gene_id`),
  INDEX `fk_reference_gene_metadata_reference_gene1_idx` (`reference_gene_id` ASC),
  CONSTRAINT `fk_reference_gene_metadata_reference_gene1`
    FOREIGN KEY (`reference_gene_id`)
    REFERENCES `genome_engine`.`reference_gene` (`reference_gene_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `genome_engine`.`reference_gene_sequence`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `genome_engine`.`reference_gene_sequence` (
  `reference_gene_id` INT(11) NOT NULL,
  `reference_gene_sequence` LONGTEXT NOT NULL,
  INDEX `fk_reference_gene_sequence_reference_gene1_idx` (`reference_gene_id` ASC),
  UNIQUE INDEX `reference_gene_id_UNIQUE` (`reference_gene_id` ASC),
  CONSTRAINT `fk_reference_gene_sequence_reference_gene1`
    FOREIGN KEY (`reference_gene_id`)
    REFERENCES `genome_engine`.`reference_gene` (`reference_gene_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
