
import time
from numpy import array


class Gene():
    def __init__(self, name=None, chrom=None, strand=None, tx_start=None, tx_end=None, cds_start=None, cds_end=None,
                 exon_count=None, exon_starts=None, exon_ends=None, protein_id=None, align_id=None):
        def nullsafe_int(integer):
            return None if integer is None else int(integer)
        def nullsafe_int_list(integer_list):
            return None if integer_list is None else list(map(int, integer_list.rstrip(",").split(",")))

        self.name = name
        self.chrom = chrom
        self.positive_strand = (strand == "+") if isinstance(strand, str) else strand
        self.tx_start = nullsafe_int(tx_start)
        self.tx_end = nullsafe_int(tx_end)
        self.cds_start = nullsafe_int(cds_start)
        self.cds_end = nullsafe_int(cds_end)
        self.exon_count = nullsafe_int(exon_count)
        self.exon_starts = nullsafe_int_list(exon_starts)
        self.exon_ends = nullsafe_int_list(exon_ends)
        self.protein_id = protein_id
        self.align_id = align_id

    def __str__(self):
        return "Gene(name:%s, chrom:%s, strand:%s, txStart:%d, txEnd:%d, cdsStart:%d, cdsEnd:%d, " \
               "exonCount:%d, exonStarts:%s, exonEnds:%s, proteinID:%s, alignID:%s)" % (
                   self.name, self.chrom, self.positive_strand, self.tx_start, self.tx_end, self.cds_start, self.cds_end,
                   self.exon_count, str(self.exon_starts), str(self.exon_ends), self.protein_id, self.align_id)
    __repr__ = __str__


class GeneSet():
    DEBUG = False

    def __init__(self):
        self.genes_by_name = dict()
        self.gene_list = list()

    def import_tsv_file(self, filename):
        """
        This imports a TSV file
        :param filename: name of the file
        :return: the sequence object generated
        """
        try:
            start = time.time()
            print_info("Importing genes from %s" % filename, "GENE_SET")
            genes_file = open(filename)
            count = 0
            next_report = 1024
            for line in genes_file:
                self.import_tsv_line(line)

                count += 1
                if GeneSet.DEBUG and count == next_report:
                    print_info("Read line %d of file %s" % (count, filename), "GENE_SET")
                    next_report *= 2

            return self
        finally:
            print_info("Read %d lines in %d seconds" % (count, int(time.time() - start)), "GENE_SET")
            if genes_file is not None: genes_file.close()

    def import_tsv_line(self, line):
        # sanity checks
        if line is None: raise Exception("Empty 'line' parameter")
        if line.lstrip().startswith("#"): return
        pieces = line.split('\t')
        if len(pieces) != 12: raise Exception("'line' parameter has unexpected tab count")

        # format of file:
        # name	chrom	strand	txStart	txEnd	cdsStart	cdsEnd	exonCount	exonStarts	exonEnds	proteinID	alignID
        gene_info = {
            'name' : pieces[0],
            'chrom' : pieces[1],
            'strand' : pieces[2],
            'tx_start' : pieces[3],
            'tx_end' : pieces[4],
            'cds_start' : pieces[5],
            'cds_end' : pieces[6],
            'exon_count' : pieces[7],
            'exon_starts' : pieces[8],
            'exon_ends' : pieces[9],
            'protein_id' : pieces[10],
            'align_id' : pieces[11].rstrip(),
        }
        # use None for not set
        for key in gene_info.keys():
            if gene_info[key] is None or len(gene_info[key].strip()) == 0: gene_info[key] = None

        # make a gene object
        gene = Gene(**gene_info)

        # save it
        self.add_gene(gene)

    def add_gene(self, gene):
        name = gene.name
        if (name in self.genes_by_name):
            print_warn("Gene '" + name + "' already added to GeneSet!", "GENE_SET")
        self.genes_by_name[name] = gene
        self.gene_list.append(gene)

    def get_gene(self, gene_name):
        if gene_name in self.genes_by_name:
            return self.genes_by_name[gene_name]
        return None

    def __getitem__(self, item):
        return self.gene_list[item]

    def __len__(self):
        return 0 if self.gene_list == None else len(self.gene_list)

    def __str__(self):
        string = ""
        names = list(self.genes_by_name.keys())
        names.sort()
        for n in names:
            if string != "": string += "\n\t"
            string += str(self.genes_by_name[n])
        return ("GeneSet(\n\t" + string + "\n)")


class Sequence:
    DEBUG = False
    def __init__(self):
        self.sequence_array = None
        self.sequence_len = None
        self.max_index = None

    def import_sequence_file(self, filename):
        try:
            print_info("Importing sequence from %s" % filename, "SEQUENCE")
            start = time.time()
            count = 0
            sequence_list = list()
            sequence_file = open(filename)
            found_comment_line = False
            self.sequence_length = None
            last_line = None
            next_report = 1024
            for line in sequence_file:
                # do this check here, so we don't print an error at end of file
                if self.sequence_length != None and len(last_line) != self.sequence_length:
                    print_warn("Expected line of length %d (line %d): %s" % (len(line), count - 1, line), "SEQUENCE")

                # comment lines
                if line.lstrip().startswith(">"):
                    if found_comment_line:
                        print_info("Found second comment line: " + line, "SEQUENCE")
                    found_comment_line = True
                    continue

                # cleanup and data gathering
                line = line.strip()
                if self.sequence_length == None:
                    self.sequence_length = len(line)

                # save line
                sequence_list.append(line)
                last_line = line

                # iterate and report
                count += 1
                if Sequence.DEBUG and count == next_report:
                    print_info("Read line %d of file %s" % (count, filename), "SEQUENCE")
                    next_report *= 2

            # convert to array and cleanup
            self.sequence_array = array(sequence_list)
            self.max_index = self.sequence_length * (len(sequence_list) - 1) + len(last_line)

            return self
        finally:
            print_info("Read %d lines in %d seconds" % (count, int(time.time() - start)), "SEQUENCE")
            if sequence_file is not None: sequence_file.close()


    def get_subsequence(self, start_idx, end_idx):
        # special use case
        if end_idx == None:
            print_warn("get_subsequence: Got query with null end_idx, using self.max_index", "SEQUENCE")
            end_idx = self.max_index

        # sanity check
        if start_idx >= end_idx: raise Exception("Invalid start/end index: %d / %d" % (start_idx, end_idx))
        if start_idx < 0 or start_idx > self.max_index: raise Exception("Invalid start index: %d" % (start_idx))
        if end_idx < 0 or end_idx > self.max_index: raise Exception("Invalid end index: %d" % (end_idx))

        start_idx = int(start_idx)
        end_idx = int(end_idx)
        start_array_idx = int(start_idx / self.sequence_length)
        start_elem_idx = int(start_idx % self.sequence_length)
        end_array_idx = int(end_idx / self.sequence_length)
        end_elem_idx = int(end_idx % self.sequence_length)

        if start_array_idx == end_array_idx:
            return self.sequence_array[start_array_idx][start_elem_idx:end_elem_idx]

        sequence = self.sequence_array[start_array_idx][start_elem_idx:]
        curr_array_idx = start_array_idx + 1
        while curr_array_idx != end_array_idx:
            sequence += self.sequence_array[curr_array_idx]
            curr_array_idx += 1
        sequence += self.sequence_array[end_array_idx][:end_elem_idx]
        return sequence


class GeneChunk():
    def __init__(self, genome_id, gene_count, chromosome, positive_strand, start_position, end_position):
        self.genome_id = genome_id
        self.gene_count = gene_count
        self.chromosome = chromosome
        self.positive_strand = positive_strand
        self.start_position = start_position
        self.end_position = end_position

    def __str__(self):
        return "GeneChunk(genome_id:%s, gene_count:%s, chromosome:%s, positive_strand:%s, start_position:%s, end_position:%s)" % (self.genome_id, self.gene_count, self.chromosome, self.positive_strand , self.start_position, self.end_position)

    __repr__ = __str__
