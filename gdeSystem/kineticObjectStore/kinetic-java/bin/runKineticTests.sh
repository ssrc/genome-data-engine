#! /usr/bin/env bash

if [ $# -eq 1 ]; then
   cd ../kinetic-test
   exec mvn -DRUN_AGAINST_EXTERNAL=true -DKINETIC_HOST=127.0.0.1 -Dtest=$1 test
elif [ $# -eq 0 ]; then
   cd ../kinetic-test
   exec mvn -DRUN_AGAINST_EXTERNAL=true -DKINETIC_HOST=127.0.0.1 test
else
   echo "bad usage"
fi

