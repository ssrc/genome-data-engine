package com.seagate.kinetic.simulator.persist.multileveldb;

import java.io.IOException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.SortedMap;
import java.util.TreeMap;

import org.iq80.leveldb.DB;

import com.google.protobuf.ByteString;

/**
 * Consistent hashing implementation based on Amazon's DynamoDB. Code
 * adapted from Tom White.
 * 
 * http://www.tom-e-white.com/2007/11/consistent-hashing.html
 */
public class ConsistentHashRing {
	
	private static final double INTMAX = Math.pow(2, 32);
	private MessageDigest md;
	private final SortedMap<Integer, DB> ring = new TreeMap<Integer, DB>();
	
	public ConsistentHashRing(List<DB> dbList) throws IOException {
		try {
			this.md = MessageDigest.getInstance("md5");
		}catch (NoSuchAlgorithmException e) {
			//
		}
		for (int i = 0; i < dbList.size(); i++) {
			int offset = (int)INTMAX / dbList.size() * i;
			ring.put(Integer.MIN_VALUE + offset, dbList.get(i));
		}
	}
	
	/**
	 * Remove specified DB from the ring
	 * 
	 * @param db
	 * 			the target DB to remove
	 */
	public void removeNode(DB db) throws IOException {
		ring.values().remove(db);
	}
	
	/**
	 * @return number of nodes on the ring
	 */
	public int size() {
		return ring.size();
	}
	
	/**
	 * @return ArrayList of all DB on the ring
	 */
	public ArrayList<DB> getAllDB() {
		ArrayList<DB> allDB = new ArrayList<DB>(ring.size());
		for(Map.Entry<Integer, DB> entry : ring.entrySet()) {
			allDB.add(entry.getValue());
		}
		return allDB;
	}
	
	/**
	 * Pick closest DB to the key's hashed position on the ring
	 * 
	 * @param key
	 * 			the key of the entry requested for put, get or delete
	 * 
	 * @return first DB found walking clockwise from key's hashed position
	 */
	public DB getNode(ByteString key) {
		if (ring.isEmpty()) {
	      return null;
	    }
	    
		byte[] keyArray = key.toByteArray();
		Integer ringKey = md5(keyArray);
		
	    if (!ring.containsKey(ringKey)) {
	      SortedMap<Integer, DB> tailMap = ring.tailMap(ringKey);
	      ringKey = tailMap.isEmpty() ? ring.firstKey() : tailMap.firstKey();
	    }
	    return ring.get(ringKey);
	} 
	
	/**
	 * Pick closest 3 DBs to the key's hashed position on the ring
	 * 
	 * @param key
	 * 			the key of the entry requested for put, get or delete
	 * 
	 * @return List of first three DB found walking clockwise from key's hashed position
	 */
	public ArrayList<DB> getThreeNodes(ByteString key) {
		ArrayList<DB> threeDB = new ArrayList<DB>(3);
		
		if (ring.isEmpty()) {
	      return null;
	    }
	    
		byte[] keyArray = key.toByteArray();
		Integer ringKey = md5(keyArray);
		
        SortedMap<Integer, DB> tailMap = ring.tailMap(ringKey);
        for (int i = 0; i < 3; i++) {
        	ringKey = tailMap.isEmpty() ? ring.firstKey() : tailMap.firstKey();
        	threeDB.add(ring.get(ringKey));
        	tailMap = ring.tailMap(ringKey);
        }
	    
	    return threeDB;
	} 
	
	/**
	 * Hash the given key using MD5 and truncate result to fit into <code>Integer</code>
	 * 
	 * @param key
	 * 			the key of the entry requested for put, get or delete
	 * 
	 * @return Integer representing hashed position on the ring
	 */
	private Integer md5(byte[] key) {
		md.update(key);
		byte[] hash = md.digest();
		Integer ringKey = ((hash[0] & 0xFF) << 24 | (hash[1] & 0xFF) << 16 | (hash[2] & 0xFF) << 8 | (hash[3] & 0xFF) << 0);
		return ringKey;
	}
	
	
	
}
